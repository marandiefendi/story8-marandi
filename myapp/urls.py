from django.urls import path
from .views import story8, data

app_name = 'myapp'

urlpatterns = [
    path('', story8),
    path('data',data)
]