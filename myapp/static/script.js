$(document).ready(function() {
        var q = "kimia"
        console.log(q)
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + q,
            success: function(data) {
                $('#konten').html('')
                var result = '<tr>';
                for (var i = 0; i < data.items.length; i++) {
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>";
                }
                $('#konten').append(result);
            },
            error: function(error) {
                alert("Books not found");
            }
        })
        $("#button").on('click', function() {
            var q = $("#input").val().toLowerCase()
            console.log(q)
            $.ajax({
                url: "https://www.googleapis.com/books/v1/volumes?q=" + q,
                success: function(data) {
                    $('#konten').html('')
                    var result = '<tr>';
                    for (var i = 0; i < data.items.length; i++) {
                        result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                            "<td><img class='img-fluid' style='width:22vh' src='" +
                            data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>";
                    }
                    $('#konten').append(result);
                },
                error: function(error) {
                    alert("Books not found");
                }
            })
        });

});
