from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from .views import story8
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

class Story8_Test(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_html_exist(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story8.html')

    def test_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, story8)

    def test_url_not_exist(self):
        response = Client().get('/oi')
        self.assertEqual(response.status_code, 404)

class TestWithSelenium(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)

    def test_cari_buku(self):
        self.driver.get("http://localhost:8000")
        self.driver.find_element_by_id('input').send_keys("fisika")
        self.driver.find_element_by_id('button').click()


    def tearDown(self):
        self.driver.quit


